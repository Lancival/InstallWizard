<html>
<p><b>After downloading this game, please enter the src directory.</b> To compile the game, use the "javac installwizard/*.java" command from the src directory. To run the game, use the "java installwizard/Game" command from the src directory.</p>
<p>InstallWizard is a typing RPG game, in which you prepare and cast spells by typing on the keyboard in order to defeat approaching enemies.</p>
<ul>
    <li>Each installwizard.spell can damage enemies or provide other effects </li>
    <li>Enemies will attack after a certain period of time if they are not defeated quickly enough </li>
    <li>Enemies will grow stronger and spells may become more complex as time goes on </li> 
</ul>
Design Plan:
<ul>
    <li>Game class: initializes the game</li>
    <li>Board class: draws and runs the game</li>
    
    <li>Entity (abstract):
    <ul>
    <li>Every installwizard.entity has health, which decreases with attacks</li>
    <li>Characters</li>
    <ul>
        <li>Different characters may have different themes</li>
        <li>Includes list of Spells</li>
    </ul>
    <li>Monsters</li>
    <ul>
        <li>Attack after a certain period of time if not defeated</li>
    </ul>
    </ul>
    </li>
    
    <li>Spell (abstract):
    <ul>
    <li>Includes list of instructions</li>
    </ul>
    </li>
</ul>
<p>Note: Code for spells, entities, and wizards compile, but have not yet tested whether they actually work.</p>
<p>Changelist (most recent to top): </p>
<ul>
    <li>Decided on new format for Spell.java (ask Richard for specifics)</li>
</ul>
</html>