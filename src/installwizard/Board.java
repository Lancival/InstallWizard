package installwizard;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import java.net.URL;
import javax.sound.sampled.*;
import installwizard.entity.*;
import installwizard.spell.*;

public class Board extends JPanel implements Runnable {
	private final int B_WIDTH; //Width of screen
	private final int B_HEIGHT; //Height of screen
	private final int DELAY = 25; //Animation/gameplay speed

	private BufferedImage title; //Title image
	private Thread animator; //Animates the game
	private Font special; //Retro Rescued font, does not support Unicode characters
	private Font normal = new Font("Lucida Grande", Font.PLAIN, 14); //Does not support Unicode characters
	private static String gameplay = "title"; //Represents which "screen" the game should display
	private JMenuBar menu; //Menu containing pause/resume and return to title screen buttons
	private int[] highscores = new int[10]; //List of highscores
	private String[] names = new String[10]; //List of players' names who achieved the corresponding highscores
	private static boolean pause = true; //Represents whether the game is paused or not
	private BooleanControl mute; //Represents whether the background music is muted or not
	private Wizard example = new Example(); //Example wizard for the How to Play screen
	private Wizard[] wizards = new Wizard[4]; //List of four wizards controlled during the actual game
	private Wizard[] wizardList = {new Rasputin(), new Redactamancer(), new Dragonborn(), new Sourcerer(), new Ozymandias(), new Kurenai()}; //List of different types of wizards who can be selected
	private Enemy[] enemies = new Enemy[4]; //List of enemies the wizards are currently fighting during gameplay
	private Enemy[] enemyList = {new Goblin(), new Whirlwind(), new Hydra(), new Jabberwocky(), new Orc(), new Berserker()}; //List of different types of enemies that the wizards may fight
	private int score = 0; //Number of monsters vanquished during the game
	private String name = ""; //Name of player

	public Board(JMenuBar mb) throws IOException {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); //Detects size of user's screen
		B_WIDTH = (int) screenSize.getWidth(); //Sets width of window to the width of the user's screen
		B_HEIGHT = (int) screenSize.getHeight(); //Sets height of window to the height of the user's screen
		menu = mb; //Sets menu the JMenuBar provided by the Game class
		initBoard(); //Initializes window upon creating a Board object
	}

	private void initBoard() throws IOException {
		//Initializes the size, background, images, keyboard listener, mouse listener, and high scores
		addKeyListener(new TAdapter()); //Adds a keyboard listener to detect key presses and characters typed
		addMouseListener(new MouseListener() { //Adds a mouse listener to detect clicks on the screen
			@Override
			public void mouseClicked(MouseEvent e) {
				int x = e.getX(); //x-value of where the mouse clicked
				int y = e.getY(); //y-value of where the mouse clicked

				//If on the title screen, go to different "screens" if the appropriate button is clicked
				if (gameplay == "title") {
					if (x>=(B_WIDTH-500)/2 && x<=((B_WIDTH-500)/2+500)) {
						if (y>=(B_HEIGHT-80)/2-75 && y<=(B_HEIGHT-80)/2-25) {
							wizards = new Wizard[4];
							gameplay = "select";
						}
						else if (y>=(B_HEIGHT-80)/2 && y<=(B_HEIGHT-80)/2+50) {
							example = new Example();
							gameplay = "instructions";
						}
						else if (y>=(B_HEIGHT-80)/2+75 && y<=(B_HEIGHT-80)/2+125) gameplay = "options";
						else if (y>=(B_HEIGHT-80)/2+150 && y<=(B_HEIGHT-80)/2+200) {
							loadHighScores();
							gameplay = "highscores";
						}
						else if (y>=(B_HEIGHT-80)/2+225 && y<=(B_HEIGHT-80)/2+275) gameplay = "credits";
					}
				}
				//Mute the background music if the mute button is clicked
				else if (gameplay == "options") if (x>=(B_WIDTH-250)/2 && x<=(B_WIDTH-250)/2+250 && y>=(B_HEIGHT-80)/4+50 && y<=(B_HEIGHT-80)/4+100) mute.setValue(!mute.getValue());
			}

			@Override
			public void mouseExited(MouseEvent e) { //Not used
				return;
			}

			@Override
			public void mouseEntered(MouseEvent e) { //Not used
				return;
			}

			@Override
			public void mouseReleased(MouseEvent e) { //Not used
				return;
			}

			@Override
			public void mousePressed(MouseEvent e) { //Not used
				return;
			}
		});

		setFocusable(true); //Allows board to be focusable
		setBackground(Color.WHITE); //Set background color of window to white
		setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT)); //Sets size of the window to the size of the computer's screen
		setDoubleBuffered(true); //Makes animation smoother
		loadImages(); //Loads images used in the game
		loadSounds(); //Loads and starts playing background music
		loadFonts(); //Loads the retro rescued fonts
		loadHighScores(); //Loads the list of high scores stored in a text file
	}

	public static void returnToTitle() {
		//Pauses the game and returns the game to the title screen when called
		gameplay = "title";
		pause = true;
	}

	public static void pause() {
		//Pauses or unpauses the game when called (used only by the Game class)
		pause = !pause;
	}

	public void writeHighScores() {
		//Rewrites the highscores text file to update the high scores
		loadHighScores(); //First loads existing high scores

		//Determines whether the current score achieved is a high score, and if so, what rank the score is. Updates the highscores array as it finds the position for the current score.
		for (int i=9; i>0; i--) {
			if (score > highscores[i]) {
				highscores[i] = highscores[i-1];
				names[i] = names[i-1];
			}
			else if (i != 9) {
				highscores[i] = highscores[i+1];
				names[i] = names[i+1];
				highscores[i+1] = score;
				names[i+1] = name;
				break;
			}
		}
		if (score > highscores[0]) {
			highscores[0] = score;
			names[0] = name;
		}

		//Using the updated highscores list, writes the new highscores with their corresponding names into the highscores text file
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File("installwizard/highscores.txt")));
			for (int i=0; i<highscores.length; i++) bufferedWriter.write(names[i] + " " + highscores[i] + "\n", 0, (names[i] + " " + highscores[i] + "\n").length());
			bufferedWriter.close();
		} catch (IOException e) {
			String msg = String.format("Error reading file: %s", e.getMessage());
			JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void loadImages() throws IOException {
		//Loads images needed for the game (currently only the title image)
		title = ImageIO.read(new File("installwizard/Images/Title.png"));

	}

	private void loadSounds() throws IOException {
		//Loads and plays the background music
    	try {
		    //URL url = new URL("http://static1.grsites.com/archive/sounds/cartoon/cartoon001.wav");
		    URL url = this.getClass().getClassLoader().getResource("installwizard/Sounds/Background.wav"); //Placeholder music, to be replaced if time allows
		    Clip clip = AudioSystem.getClip();
		    AudioInputStream ais = AudioSystem.getAudioInputStream(url);
		    clip.open(ais);
		    mute = (BooleanControl) clip.getControl(BooleanControl.Type.MUTE);
		    mute.setValue(false); //Background music is audible by default
		    clip.loop(5); //Sets background music to loop forever
		} catch (UnsupportedAudioFileException e) {
			String msg = String.format("Unsupported Audio File: %s", e.getMessage());
			JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
		} catch (LineUnavailableException e) {
        	String msg = String.format("Line Unavailable: %s", e.getMessage());
			JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void loadFonts() throws IOException {
		//Loads the Retro Rescued font, which is the special font
		try {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("installwizard/Fonts/Retro_Rescued.ttf")));
		} catch (FontFormatException e) {
			String msg = String.format("Incorrect Font Format: %s", e.getMessage());
			JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
		}

		special = new Font("Retro Rescued", Font.PLAIN, 32);
	}

	private void loadHighScores() {
		//Loads highscores from the text file
		try {
			FileReader fileReader = new FileReader("installwizard/highscores.txt");
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			for (int i=0; i<10; i++) {
				String[] score = bufferedReader.readLine().split(" ");
				highscores[i] = Integer.parseInt(score[1]);
				names[i] = score[0];
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			String msg = String.format("Could not find file: %s", e.getMessage());
			JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			String msg = String.format("Could not read file: %s", e.getMessage());
			JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public void addNotify() {
		//Initializes animation
		super.addNotify();
		animator = new Thread(this);
		animator.start();
	}

	@Override
	public void paintComponent(Graphics g) {
		//Gives actual job of painting to draw___() methods
		super.paintComponent(g);
		if (gameplay == "gameplay") drawGame(g);
		else if (gameplay == "select") drawSelect(g);
		else if (gameplay == "credits") drawCredits(g);
		else if (gameplay == "highscores") drawScores(g);
		else if (gameplay == "instructions") drawInstructions(g);
		else if (gameplay == "options") drawOptions(g);
		else if (gameplay == "game over") drawGameOver(g);
		else drawTitle(g);
	}

	private void drawSelect(Graphics g) {
		//Draws lists of wizards that players can select by typing

		//Divides screen into four quadrants
		g.setColor(Color.BLACK);
		g.drawLine(B_WIDTH/2,0,B_WIDTH/2,B_HEIGHT);
		g.drawLine(0,(B_HEIGHT-80)/2,B_WIDTH,(B_HEIGHT-80)/2);

		//Draws the "Choose Your Wizards" button
		drawButton(g,B_WIDTH/2,(B_HEIGHT-80)/2,500,50,"Choose Your Wizards");

		//For each wizard, draw the wizard's name if it has been selected. Otheriwse, draw a list of wizards to choose from.
		if (wizards[0] == null) drawWizardList(g, 20, 50);
		else {
			g.setFont(special);
			g.drawString(wizards[0].getName(), 20, 50);
		}
		if (wizards[1] == null) drawWizardList(g, B_WIDTH/2+20, 50);
		else {
			g.setFont(special);
			g.drawString(wizards[1].getName(), B_WIDTH/2+20, 50);
		}
		if (wizards[2] == null) drawWizardList(g, 20, (B_HEIGHT-80)/2+50);
		else {
			g.setFont(special);
			g.drawString(wizards[2].getName(), 20, (B_HEIGHT-80)/2+50);
		}
		if (wizards[3] == null) drawWizardList(g, B_WIDTH/2+20, (B_HEIGHT-80)/2+50);
		else {
			g.setFont(special);
			g.drawString(wizards[3].getName(), B_WIDTH/2+20, (B_HEIGHT-80)/2+50);
		}
	}

	private void drawWizardList(Graphics g, int x, int y) {
		//Draws a list of wizards to choose from in the specified location
		g.setFont(normal);
		g.setColor(Color.BLACK);
		g.drawString("Available Wizards:", x, y);
		String name = "";
		for (int i=0; i<wizardList.length; i++) { //Need to change so that players must select unique wizards
			name = wizardList[i].getName();
			boolean taken = false;
			for (Wizard wizard : wizards) if (wizard != null && wizard.getName() == name) taken = true;
			if (taken) g.setColor(Color.LIGHT_GRAY);
			else g.setColor(Color.BLACK);
			g.drawString("[" + name.substring(0,1) + "] " + name, x, y+g.getFontMetrics(normal).getHeight()*(i+1));
		}
	}

	private void drawSpellList(Graphics g, int x, int y, Spell[] spellBook) {
		//Draws a list of spells to cast for a wizard
		g.setFont(normal);
		g.setColor(Color.BLACK);
		g.drawString("You begin to cast: ", x, y);
		String name = "";
		for (int i=0; i<spellBook.length; i++) {
			name = spellBook[i].getName();
			g.drawString("[" + name.substring(0,1) + "] " + name, x, y+g.getFontMetrics(normal).getHeight()*(i+1));
		}
	}

	private void drawGame(Graphics g) {
		//Draws the actual gameplay

		//Divides the screen into four quadrants
		g.setColor(Color.BLACK);
		g.drawLine(B_WIDTH/2,0,B_WIDTH/2,B_HEIGHT);
		g.drawLine(0,(B_HEIGHT-80)/2,B_WIDTH,(B_HEIGHT-80)/2);

		Toolkit.getDefaultToolkit().sync();
		drawHealth(g); //Draws the health bars of all entities currently in the game

		//Draws graphics
		drawSprites(g);
		
		//Draws the names of all entities currently in the game
		g.setColor(Color.BLACK);
		g.setFont(special);
		g.drawString(wizards[0].getName(), 10, 20+g.getFontMetrics(special).getHeight());
		g.drawString(wizards[1].getName(), B_WIDTH/2+10, 20+g.getFontMetrics(special).getHeight());
		g.drawString(wizards[2].getName(), 10, (B_HEIGHT-80)/2+20+g.getFontMetrics(special).getHeight());
		g.drawString(wizards[3].getName(), B_WIDTH/2+10, (B_HEIGHT-80)/2+20+g.getFontMetrics(special).getHeight());
		g.drawString(enemies[0].getName(), 10, B_HEIGHT/2-40-g.getFontMetrics(special).getHeight());
		g.drawString(enemies[1].getName(), B_WIDTH/2+10, B_HEIGHT/2-40-g.getFontMetrics(special).getHeight());
		g.drawString(enemies[2].getName(), 10, B_HEIGHT-80-g.getFontMetrics(special).getHeight());
		g.drawString(enemies[3].getName(), B_WIDTH/2+10, B_HEIGHT-80-g.getFontMetrics(special).getHeight());

		//Draws each wizard's current spell, or if the current spell is complete, draw the spellList to choose a spell. Draws "Incapacitated" if the wizard's health reaches 0.
		if (wizards[0].getHealth() == 0) {
			g.setFont(special);
			g.setColor(Color.BLACK);
			g.drawString("Incapacitated", B_WIDTH/4-g.getFontMetrics(special).stringWidth("Incapacitated")/2, (B_HEIGHT-80)/4-g.getFontMetrics(special).getHeight()/2);
		}
		else if (wizards[0].getPage() != null && !wizards[0].getPage().isComplete()) drawSpell(g, 10, 40+g.getFontMetrics(special).getHeight()+5, B_WIDTH/2-20, wizards[0].getPage());
		else drawSpellList(g, 10, 40 + g.getFontMetrics(special).getHeight()+5, wizards[0].getSpellBook());
		if (wizards[1].getHealth() == 0) {
			g.setFont(special);
			g.setColor(Color.BLACK);
			g.drawString("Incapacitated", 3*B_WIDTH/4-g.getFontMetrics(special).stringWidth("Incapacitated")/2, (B_HEIGHT-80)/4-g.getFontMetrics(special).getHeight()/2);
		}
		else if (wizards[1].getPage() != null && !wizards[1].getPage().isComplete()) drawSpell(g, B_WIDTH/2+10, 40+g.getFontMetrics(special).getHeight()+5, B_WIDTH/2-20, wizards[1].getPage());
		else drawSpellList(g, B_WIDTH/2+10, 40+g.getFontMetrics(special).getHeight()+5, wizards[1].getSpellBook());
		if (wizards[2].getHealth() == 0) {
			g.setFont(special);
			g.setColor(Color.BLACK);
			g.drawString("Incapacitated", B_WIDTH/4-g.getFontMetrics(special).stringWidth("Incapacitated")/2, 3*(B_HEIGHT-80)/4-g.getFontMetrics(special).getHeight()/2);
		}
		else if (wizards[2].getPage() != null && !wizards[2].getPage().isComplete()) drawSpell(g, 10, (B_HEIGHT-80)/2+40+g.getFontMetrics(special).getHeight()+5, B_WIDTH/2-20, wizards[2].getPage());
		else drawSpellList(g, 10, (B_HEIGHT-80)/2+40+g.getFontMetrics(special).getHeight()+5, wizards[2].getSpellBook());
		if (wizards[3].getHealth() == 0) {
			g.setFont(special);
			g.setColor(Color.BLACK);
			g.drawString("Incapacitated", 3*B_WIDTH/4-g.getFontMetrics(special).stringWidth("Incapacitated")/2, 3*(B_HEIGHT-80)/4-g.getFontMetrics(special).getHeight()/2);
		}
		else if (wizards[3].getPage() != null && !wizards[3].getPage().isComplete()) drawSpell(g, B_WIDTH/2+10, (B_HEIGHT-80)/2+40+g.getFontMetrics(special).getHeight()+5, B_WIDTH/2-20, wizards[3].getPage());
		else drawSpellList(g, B_WIDTH/2+10, (B_HEIGHT-80)/2+40+g.getFontMetrics(special).getHeight()+5, wizards[3].getSpellBook());
	}
	
	private void drawSprites(Graphics g) {
		for (int i = 0; i < 4; i++) { drawSingle(g, wizards[i], i); }
	}

	private void drawSingle(Graphics g, Entity wizzy, int order) {
		int x, y;
		if (order == 0) {x = B_WIDTH/2 - 20; y = B_HEIGHT/2 - 60;}
		else if (order == 1) {x = B_WIDTH - 20; y = B_HEIGHT/2 - 60;}
		else if (order == 2) {x = B_WIDTH/2 - 20; y = B_HEIGHT - 60;}
		else if (order == 3) {x = B_WIDTH - 20; y = B_HEIGHT - 60;}


	}

	private void drawHealth(Graphics g) {
		//Draws the health of all entities currently in the game
		drawHealthBar(g, 10, 10, (B_WIDTH-40)/2, 20, wizards[0]);
		drawHealthBar(g, B_WIDTH/2+10, 10, (B_WIDTH-40)/2, 20,wizards[1]);
		drawHealthBar(g, 10, (B_HEIGHT-80)/2+10, (B_WIDTH-40)/2, 20, wizards[2]);
		drawHealthBar(g, B_WIDTH/2+10, (B_HEIGHT-80)/2+10, (B_WIDTH-40)/2, 20, wizards[3]);
		drawHealthBar(g, 10, (B_HEIGHT-80)/2-30, (B_WIDTH-40)/2, 20, enemies[0]);
		drawHealthBar(g, B_WIDTH/2+10, (B_HEIGHT-80)/2-30, (B_WIDTH-40)/2, 20, enemies[1]);
		drawHealthBar(g, 10, (B_HEIGHT-80)-30, (B_WIDTH-40)/2, 20, enemies[2]);
		drawHealthBar(g, B_WIDTH/2+10, (B_HEIGHT-80)-30, (B_WIDTH-40)/2, 20, enemies[3]);
	}

	private void drawHealthBar(Graphics g, int x, int y, int width, int height, Entity e) {
		//Draws a health bar based at the specified position for a given Entity
		int health = e.getHealth();
		int maxHealth = e.getMaxHealth();
		g.setColor(Color.BLACK);
		g.drawRect(x,y,width,height);

		//Fills in the health bar in a color based on the ratio of health to maxHealth - starts off green at high health, then becomes increasingly red
		g.setColor(new Color(Math.min(255-2*(int)((health*1.0/maxHealth-0.5)*255),255),(int)(health*1.0/maxHealth*255),0));
		g.fillRect(x+1,y+1,(int)((width-2)*1.0*health/maxHealth),height-2);
	}

	private void drawTitle(Graphics g) {
		//Draws the title screen

		//Draws the title image, with random offset to generate "vibration" effect
		g.drawImage(title, (B_WIDTH-title.getWidth())/2+(int)(Math.round((Math.random()*3-1))), (B_HEIGHT-80-title.getHeight())/4+(int)(Math.round((Math.random()*3-1))), this);
		
		//Draws all the buttons on the title screen
		drawButton(g, B_WIDTH/2, (B_HEIGHT-80)/2-50, 500, 50, "Play");
		drawButton(g, B_WIDTH/2, (B_HEIGHT-80)/2+25, 500, 50, "How to Play");
		drawButton(g, B_WIDTH/2, (B_HEIGHT-80)/2+100, 500, 50, "Options");
		drawButton(g, B_WIDTH/2, (B_HEIGHT-80)/2+175, 500, 50, "Highscores");
		drawButton(g, B_WIDTH/2, (B_HEIGHT-80)/2+250, 500, 50, "Credits");
	}

	private void drawButton(Graphics g, int x, int y, int width, int height, String label) {
		//Draws button with given text label at the specified position

		//Fills the body of the button with a grey color
		g.setColor(new Color(222,222,222));
		g.fillRect(x-width/2+1,y-height/2+1,width-2,height-2);

		//Draws the outer rectangle and text in black
		g.setColor(Color.BLACK);
		g.drawRect(x-width/2,y-height/2,width,height);
		g.setFont(special);
		g.drawString(label, x-g.getFontMetrics(special).stringWidth(label)/2+1, y+g.getFontMetrics(special).getAscent()/4);
	}

	private void drawCredits(Graphics g) {
		//Draws credits for the game
		g.setFont(special);
		g.drawString("Credits", B_WIDTH/2-g.getFontMetrics(special).stringWidth("Credits")/2+1, (B_HEIGHT-80)/4+g.getFontMetrics(normal).getAscent()/4);
		g.setFont(normal);
		g.drawString("Programmers:", B_WIDTH/2-g.getFontMetrics(normal).stringWidth("Programmers:")/2+1, (B_HEIGHT-80)/4+g.getFontMetrics(normal).getAscent()/4+35);
		g.drawString("Richard C.", B_WIDTH/2-g.getFontMetrics(normal).stringWidth("Richard C.")/2+1, (B_HEIGHT-80)/4+g.getFontMetrics(normal).getAscent()/4+60);
		g.drawString("Aaron C.", B_WIDTH/2-g.getFontMetrics(normal).stringWidth("Aaron C.")/2+1, (B_HEIGHT-80)/4+g.getFontMetrics(normal).getAscent()/4+85);
		g.drawString("Edward Y.", B_WIDTH/2-g.getFontMetrics(normal).stringWidth("Edward Y.")/2+1, (B_HEIGHT-80)/4+g.getFontMetrics(normal).getAscent()/4+110);
		g.drawString("Inspired by: Too Many Chefs", B_WIDTH/2-g.getFontMetrics(normal).stringWidth("Inspired by: Too Many Chefs")/2+1, (B_HEIGHT-80)/4+g.getFontMetrics(normal).getAscent()/4+160);
	}

	private void drawScores(Graphics g) {
		//Draws the highscores screen

		//Draws the word "Highscores" as a title in the specified position
		g.setFont(special);
		g.drawString("Highscores", B_WIDTH/2-g.getFontMetrics(special).stringWidth("Highscores")/2+1, (B_HEIGHT-80)/4+g.getFontMetrics(special).getAscent()/4);

		//For each highscore, draws the name and score, centered horizontally and shifted vertically to avoid overlap
		g.setFont(normal);
		for (int i=0; i<10; i++) g.drawString((names[i]+": "+highscores[i]),B_WIDTH/2-g.getFontMetrics(normal).stringWidth((names[i]+": "+highscores[i]))/2,(B_HEIGHT-80)/4+g.getFontMetrics(normal).getAscent()/4+35+25*i);
	}

	private void drawInstructions(Graphics g) {
		//Draws the How to Play screen
		g.setFont(special);
		g.drawString("How to Play", B_WIDTH/2-g.getFontMetrics(special).stringWidth("How to Play")/2+1, 40);
		g.setFont(normal);

		//Draws some explanations on the screen
		String text = "Install Wizard is a typing game in which you cast spells by typing words, phrases, and other text highlighted on the screen.";
		g.drawString(text,B_WIDTH/2-g.getFontMetrics(normal).stringWidth(text)/2+1, 50+(g.getFontMetrics(normal).getHeight()+5));
		text = "For each wizard, you will see the wizard's health bar and a set of keywords to type.";
		g.drawString(text,B_WIDTH/2-g.getFontMetrics(normal).stringWidth(text)/2+1, 50+2*(g.getFontMetrics(normal).getHeight()+5));
		text = "Below is For Example, the Illustrative Examplar. Try typing the red words to cast a spell!";
		g.drawString(text,B_WIDTH/2-g.getFontMetrics(normal).stringWidth(text)/2+1, 50+3*(g.getFontMetrics(normal).getHeight()+5));

		//Draws the example wizard's health bar, name, and spell
		drawHealthBar(g, (B_WIDTH-250)/2, 140, 250, 20, example);
		g.setFont(special);
		g.setColor(Color.BLACK);
		g.drawString(example.getName(),B_WIDTH/2-g.getFontMetrics(special).stringWidth(example.getName())/2+1,190);
		if (example.getHealth() == example.getMaxHealth()) drawSpell(g, B_WIDTH/2-245,220,490,example.getPage());
		//If the player has cast the spell, draws some more explanations
		else {
			g.setFont(normal);
			text = "Whoops, looks like you accidentally targeted yourself with that spell!";
			g.drawString(text, B_WIDTH/2-g.getFontMetrics(normal).stringWidth(text)/2+1, 215);
			text = "In the real game, you'll hit the right target automatically.";
			g.drawString(text, B_WIDTH/2-g.getFontMetrics(normal).stringWidth(text)/2+1, 240);
			text = "You'll also be controlling four wizards at once. Careful, what you type will affect all of them.";
			g.drawString(text, B_WIDTH/2-g.getFontMetrics(normal).stringWidth(text)/2+1, 265);
			text = "Finally, you'll be fighting monsters, which will attack you periodically. You lose if all of your wizards reach zero health!";
			g.drawString(text, B_WIDTH/2-g.getFontMetrics(normal).stringWidth(text)/2+1, 290);
			text = "Happy hunting!";
			g.drawString(text, B_WIDTH/2-g.getFontMetrics(normal).stringWidth(text)/2+1, 315);
		}
	}

	private void drawSpell(Graphics g, int x, int y, int width, Spell spell) {
		//Draws the given spell's instructions in the given position, with a given width. The spell's flavor text and typed portion of the sequence is in black, and the untyped portion of the sequence is in red.
		
		//Retrieves the spell's flavor text, sequence, and how much of the sequence has been typed
		String[] flavor = spell.getFlavor();
		String[] sequence = spell.getSequence();
		int s = spell.keyed();

		//Combines the flavor text and sequence into a single array, to make alternating between the two easier.
		String[] sum = new String[flavor.length + sequence.length];
		for (int j=0; j<flavor.length; j++) {
			sum[2*j] = flavor[j];
			if (j < sequence.length) sum[2*j+1] = sequence[j];
		}

		//Initializes various variables used in the main body of the drawSpell() method
		String line = ""; //Line to be drawn on the screen
		int lines = 0; //How many lines have already been drawn on the screen for this spell
		FontMetrics fm = g.getFontMetrics(normal); //FontMetrics for the normal (Lucida Grande) font
		g.setFont(normal); //Draws the spell in the normal font
		int taken = 0; //How much of the current line has already been taken by previous line(s) drawn
		int count = 0; //How much of the sequence has been incorporated into lines and/or drawn out in previous lines

		//Builds up each line character by character, until the line exceeds the alloted width, which is then cut down to size before being displayed.
		//Also switches color to display which letters/characters to type.
		for (int j=0; j<flavor.length+sequence.length; j++) {
			int i=0;
			
			//Changes the color to black if drawing flavor text, or red if drawing the keyword sequences
			g.setColor(Color.BLACK);
			if (j%2 == 1 && count >= s) g.setColor(Color.RED);
			
			//Builds up each line character by character
			while (i < sum[j].length()) {
				line += sum[j].substring(i, ++i);
				if (j%2 == 1) count++;
				
				if (fm.stringWidth(line) + taken > width) {
					int space = line.lastIndexOf(" ");
					
					//If the sequence does not contain a space, print the whole line
					if (space == -1) {
						lines++;
						g.drawString(line, x, y+(lines)*fm.getHeight());
						taken = fm.stringWidth(line);
						line = "";
					}
					//Otherwise, cut out the last word from the line and then display
					else {
						line = line.substring(0, space);
						i = sum[j].indexOf(line)+space+1;
						g.drawString(line, x+taken, y+(lines++)*fm.getHeight());
						line = "";
						taken = 0;
					}
				}
				else if (j%2 == 1 && count == s) {
					g.drawString(line, x+taken, y+(lines)*fm.getHeight());
					g.setColor(Color.RED);
					taken += fm.stringWidth(line);
					line = "";
				}
			}
			if (line.length() > 0) g.drawString(line, x+taken, y+lines*fm.getHeight());
			taken += fm.stringWidth(line);
			line = "";
		}
	}

	private void drawOptions(Graphics g) {
	    //Draws the options for the game
		g.setFont(special);
		g.drawString("Options", B_WIDTH/2-g.getFontMetrics(special).stringWidth("Options")/2+1, (B_HEIGHT-80)/4+g.getFontMetrics(special).getAscent()/4);
		
		//Draws the mute/unmute button
		drawButton(g, B_WIDTH/2, (B_HEIGHT-80)/4+75, 250, 50, (mute.getValue() ? "Unmute" : "Mute"));
	}

	private void drawGameOver(Graphics g) {
	    //Draws the game over screen
		g.setFont(special);
		g.setColor(Color.BLACK);
		g.drawString("Game Over", B_WIDTH/2-g.getFontMetrics(special).stringWidth("Game Over")/2, (B_HEIGHT-80)/4+g.getFontMetrics(special).getHeight());
		g.drawString("Score: " + score, B_WIDTH/2-g.getFontMetrics(special).stringWidth("Score: " + score)/2, (B_HEIGHT-80)/4+2*g.getFontMetrics(special).getHeight());
		
		//If a new highscore was achieved, display text
		if (score > highscores[9]) {
			g.setColor(new Color((int) Math.floor(Math.random()*256), (int) Math.floor(Math.random()*256), (int) Math.floor(Math.random()*256)));
			g.drawString("New Highscore!", B_WIDTH/2-g.getFontMetrics(special).stringWidth("New Highscore!")/2+(int) Math.floor(Math.random()*3)-1, (B_HEIGHT-80)/4+3*g.getFontMetrics(special).getHeight() + (int) Math.floor(Math.random()*3)-1);
			g.setColor(Color.BLACK);
			g.drawString("Enter your name below: ", B_WIDTH/2-g.getFontMetrics(special).stringWidth("Enter your name below: ")/2, (B_HEIGHT-80)/4+4*g.getFontMetrics(special).getHeight());
			g.drawString(name+"_", B_WIDTH/2-g.getFontMetrics(special).stringWidth(name+"_")/2, (B_HEIGHT-80)/4+5*getFontMetrics(special).getHeight());
		}
	}

	private void cycle() {
	    //Runs the game by checking for actions every 25 milliseconds (called by run())
	    
	    //Displays the menu on each screen except the title screen
		if (gameplay == "title") menu.setVisible(false);
		else menu.setVisible(true);
		
		//If the game is not paused, check whether enemies attack and whether wizards have died
		if (!pause) {
			for (int i=0; i<enemies.length; i++) {
				// call tick() first, since it may change the health of the enemy
				enemies[i].tick();
				if (enemies[i].getHealth() <= 0) {
					enemies[i] = (Enemy) enemyList[(int) Math.floor(Math.random()*enemyList.length)].copy();
					score++;
				}
				if (enemies[i].checkAttackTime()) {
					if (Character.toString(enemies[i].getTarget()).equals("e")) enemies[i].effect(wizards[i]);
					else if (Character.toString(enemies[i].getTarget()).equals("a")) enemies[i].effect(wizards);
				}
			}
			boolean lost = true;
			for (int i=0; i<wizards.length; i++) {
				wizards[i].tick();
				if (wizards[i].getHealth() != 0) lost = false;
			}
			
			//Switches to the game over screen if all four wizards are incapacitated
			if (lost) {
				pause = true;
				loadHighScores();
				gameplay = "game over";
			}
		}
	}

	@Override
	public void run() {
		long beforeTime, timeDiff, sleep;
		//Used for making the game run at a constant speed

		beforeTime = System.currentTimeMillis();

		while (true) {
			cycle();
			repaint();

			timeDiff = System.currentTimeMillis() - beforeTime;
			sleep = DELAY - timeDiff;
			if (sleep < 0) sleep = 2;

			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				String msg = String.format("Thread interrupted: %s", e.getMessage());
				JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			beforeTime = System.currentTimeMillis();
		}
	}

	private class TAdapter extends KeyAdapter {
	    //Listens for key types
		@Override
		public void keyTyped(KeyEvent e) {
			Character key = e.getKeyChar();
			if (gameplay == "instructions" && example.getPage().next(key)) example.getPage().effect(example);
			//Select characters by typing the first letter of their names
			else if (gameplay == "select") {
				for (Wizard w : wizardList) {
					if (w.getName().substring(0,1).equals(Character.toString(key))) {
						if (wizards[0] == null) wizards[0] = (Wizard) w.copy();
						else if (wizards[1] == null) wizards[1] = (Wizard) w.copy();
						else if (wizards[2] == null) wizards[2] = (Wizard) w.copy();
						else { 
							wizards[3] = (Wizard) w.copy();
							for (int i=0; i<enemies.length; i++) enemies[i] = (Enemy)(enemyList[(int) Math.floor(Math.random()*enemyList.length)]).copy();
							score = 0;
							name = "";
							gameplay = "gameplay";
							pause = false;
						}
					}
				}
			}
			//Checks whether they keywords of spells were typed, and activates appropriate effects when spells completed
			else if (gameplay == "gameplay" && pause == false) {
				for (int i=0; i<wizards.length; i++) {
					if (wizards[i].getHealth() > 0) {
						Spell s = wizards[i].getPage();
						if (s != null && !s.isComplete()) {
							if (s.next(key)) {
								String type = Character.toString(s.getTarget());
								if (type.equals("s")) s.effect(wizards[i]);
								else if (type.equals("t")) s.effect(wizards);
								else if (type.equals("e")) s.effect(enemies[i]);
								else if (type.equals("a")) s.effect(enemies);
								else if (type.equals("d")) {
									Entity[] targets = {wizards[i], enemies[i]};
									s.effect(targets);
								}
								else if (type.equals("r")) {
									int r = (int) (Math.floor(Math.random()*(wizards.length+enemies.length)));
									if (r >= wizards.length) s.effect(enemies[r-wizards.length]);
									else s.effect(wizards[r]);
								}
							}
						}
						else {
							Spell[] spellBook = wizards[i].getSpellBook();
							for (Spell spell : spellBook) if (spell.getName().substring(0,1).equals(Character.toString(key))) wizards[i].setPage(spell.copy());
						}
					}
				}
			}
			//Allows player to enter their name when they achieve a new highscore
			else if (gameplay == "game over") {
				if (!(key.equals("\r\n") || Character.toString(key).equals("\b"))) name += Character.toString(key);
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			/*if (key == KeyEvent.VK_LEFT) System.out.println("Left");
			else if (key == KeyEvent.VK_RIGHT) System.out.println("Right");
			else if (key == KeyEvent.VK_UP) System.out.println("Up");
			else if (key == KeyEvent.VK_DOWN) System.out.println("Down");
			else if (key == KeyEvent.VK_CAPS_LOCK) System.out.println("CAPSLOCK");*/
			
			//If on the gameplay screen, press backspace to delete the last character entered for your name, and enter to submit your name and return to the title screen
			if (gameplay == "game over") {
				if (key == KeyEvent.VK_DELETE || key == KeyEvent.VK_BACK_SPACE) {
					if (!name.equals("")) name = name.substring(0, name.length()-1);
				}
				else if (key == KeyEvent.VK_ENTER) {
					writeHighScores();
					score = 0;
					name = "";
					Board.returnToTitle();
				}
			}
			//Listens for directional keys and CAPSLOCK presses
		}
	}
}