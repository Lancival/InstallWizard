package installwizard;

import installwizard.Board;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.IOException;

public class Game extends JFrame {
	public Game() {
		initUI();
	}

	private void initUI() {
		JMenuBar mb = new JMenuBar();
		JMenuItem pause = new JMenuItem("Pause/Resume");
		//JMenuItem restart = new JMenuItem("Restart");
		JMenuItem titleScreen = new JMenuItem("Return to Title");
		pause.setHorizontalAlignment(SwingConstants.RIGHT);
		//restart.setHorizontalAlignment(SwingConstants.RIGHT);
		titleScreen.setHorizontalAlignment(SwingConstants.RIGHT);
		pause.addActionListener(new MenuListener());
		//restart.addActionListener(new MenuListener());
		titleScreen.addActionListener(new MenuListener());
		JPanel p = new JPanel();
		//JPanel r = new JPanel();
		JPanel t = new JPanel();
		p.add(pause);
		//r.add(restart);
		t.add(titleScreen);
		mb.add(p); mb.add(t);
		//mb.add(r);
		mb.setVisible(false);

		try {
			add(new Board(mb));
		} catch (IOException e) {
			String msg = String.format("Could not load file: %s", e.getMessage());
			JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

		setResizable(true);
		pack();
		setTitle("Install Wizard");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);

		setJMenuBar(mb);
	}

	public static void main (String args[]) {
		EventQueue.invokeLater(() -> {
			Game ex = new Game();
			ex.setVisible(true);
		});
	}

	private class MenuListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//System.out.println("Selected: " + e.getActionCommand());
			if (e.getActionCommand() == "Return to Title") Board.returnToTitle();
			if (e.getActionCommand() == "Pause/Resume") Board.pause();
		}
	}
}