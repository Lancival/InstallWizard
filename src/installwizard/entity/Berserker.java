package installwizard.entity;

public class Berserker extends Enemy {

	private static int attackDelay = 5000;

	public Berserker() {
		super(80, 80, "Berserk Gnoll", attackDelay, 'e');
	}

	public void effect (Entity target) {
		// Check if the reference to health works
		target.changeHealth(-10);
		if (getHealth() < 20) target.changeHealth(-20);
		// Triple damage
	}

	public void effect(Entity[] e) {
		return;
	}

	public Entity copy() {
		return new Berserker(); 
	}
}
