package installwizard.entity;

import installwizard.spell.Spell;
import installwizard.spell.Incantation;

public class Dragonborn extends Wizard {
    // Fast all-around dealer and healer with a strong special
    
    private static Spell[] spellBook = {new Incantation(), new Incantation(1)};

    public Dragonborn() {
        super(220, 220, "Xarvarax the Dragonborn", spellBook);
    }

    public Entity copy() {
    	return new Dragonborn();
    }
}
