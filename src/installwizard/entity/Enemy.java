package installwizard.entity;

public abstract class Enemy extends Entity {

	protected int attackDelay;// time between attacks
	protected int time = 0;// time since last attack
	protected char target;// wizard being attacked

	public Enemy(int h, int m, String n, int d, char t) {
		super(h, m, n);
		attackDelay = d;
		target=t;
	}

	public Enemy(Enemy e) {
		//copy constructor
		super(e.getHealth(), e.getMaxHealth(), e.getName());
		attackDelay = e.getAttackDelay();
		target=e.getTarget();
	}

	public void tick() {
		//keeps track of time since last attack
		super.tick();
		time += 25;
	}

	public int getTime() {
		//returns time since last attack
		return time;
	}
	
	public void setTime(int value) {
		// sets time
		time = value;
	}

	public int getAttackDelay() {
		// gets attack delay
		return attackDelay;
	}

	public void setAttackDelay(int value) {
		// sets attack delay
		attackDelay = value;
	}

	public abstract void effect(Entity e);
	public abstract void effect(Entity[] e);

	public boolean checkAttackTime(){
		//checks if it's time to attack
		boolean ans=(attackDelay==time);
		if (ans){
			time=0;
		}
		return ans;
	}

	public char getTarget() {
		//gets it's target
		return target;
	}
}