package installwizard.entity;

import installwizard.spell.Spell;
import java.util.ArrayList;

//Entities include Wizards and Enemies
public abstract class Entity {
  protected int health; //Current health of the Entity
  protected int maxHealth; //Maximum health of the Entity
  protected String name; //Name of the Entity
  protected ArrayList<int[]> dots = new ArrayList<>(); //int[0] is damage amount int[1] is duration

  public Entity(int h, int m, String n) {
    health = h;
    maxHealth = m;
    name = n;
  }

  public void changeHealth(int delta) {
    //Changes health by the indicated amount
    if (health <= 0) return; //Cannot change health if already dead
    health += delta;
    if (health < 0) health = 0; //Health may not fall below zero
    if (health > maxHealth) health = maxHealth; //Health may not exceed maxHealth
  }

  public void changeMax(int delta) {
    //Changes the maximum health of the Entity
    health += delta;
  }

  public void setHealth(int value) {
    //Sets the current health of the Entity
    health = Math.min(value,maxHealth);
  }
  
  public void resurrect(float percentage) {
    //If an Entity is dead, resurrects the entity with a percentage of their maximum health
    if (health == 0) health = (int) Math.floor(percentage*maxHealth);
  }

  public int getHealth() {
    //Returns the entity's current health
    return health;
  }

  public int getMaxHealth() {
    //Returns the entity's maximum health
    return maxHealth;
  }

  public String getName() {
    //Return's the entity's name
    return name;
  }

  public abstract Entity copy(); //Factory method that creates a copy of this entity

  public void tick() {
    //keeps track of damage over times on enemy
    for (int[] bleed : dots) {
      bleed[1]--;
      if (bleed[1]%40 == 0) changeHealth(-bleed[0]);
    }
    for (int i = dots.size() - 1; i >= 0; i--) if (dots.get(i)[1] <= 0) dots.remove(i); //do damage, then remove bleeding when done
  }
  
  public void addDot(int damage, int duration) {
    //adds a damage over time effect with the provided damage and duration
    dots.add(new int[]{damage,duration*40});
  }
}