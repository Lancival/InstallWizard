package installwizard.entity;

import installwizard.spell.Spell;
import installwizard.spell.Illustration;

public class Example extends Wizard {
	
	private static Spell[] spellBook = {new Illustration()};

	public Example() {
		super(100,100,"For Example, the Illustrative Exemplar", spellBook);
		setPage(); //Necessary only for this wizard because no spell selection on the "How to Play" screen
	}

	public Entity copy() {
		return new Example();
	}
}