package installwizard.entity;

public class Goblin extends Enemy {

	private static int attackDelay = 5000;

	public Goblin() {
		super(100,100,"Goblin", attackDelay,'e');
	}

	public void effect(Entity e) {
		e.changeHealth(-20);
	}

	public void effect(Entity[] e) {
		return;
	}

	public Entity copy() {
		return new Goblin();
	}
}