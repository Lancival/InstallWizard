package installwizard.entity;

public class Hydra extends Enemy {

	private static int attackDelay = 5000;
	private static int count = 1;

	public Hydra() {
		super(25*count, 25*count, count + "-Headed Hydra", attackDelay, 'e');
	}

	public void effect(Entity target) {
		target.changeHealth(-10*count);
	}

	public void effect(Entity[] e) {
		return;
	}

	public Entity copy() {
		count++;
		return new Hydra(); 
	}
}
//Grows each time it's killed; n-headed hydra