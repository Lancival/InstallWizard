package installwizard.entity;

public class Jabberwocky extends Enemy {
	
	private static int attackDelay = 10000;

	public Jabberwocky() {
		super(200, 200, "Jabberwocky", attackDelay, 'e');
	}

	public void effect(Entity target) {
		target.changeHealth(-50);
	}

	public void effect(Entity[] e) {
		return;
	}

	public Entity copy() {
		return new Jabberwocky();
	}
}