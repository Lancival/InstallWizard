package installwizard.entity;

import installwizard.spell.Spell;
import installwizard.spell.FlameSlash;
import installwizard.spell.Impale;
import installwizard.spell.CallForMedic;

public class Kurenai extends Wizard {
	
	private static Spell[] spellBook = {new FlameSlash(), new Impale(), new CallForMedic()};

	public Kurenai() {
		super(180, 180, "Kurenai, the Spellblade", spellBook);
	}

	public Entity copy() {
		return new Kurenai();
	}
}
