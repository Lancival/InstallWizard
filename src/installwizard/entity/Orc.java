package installwizard.entity;

public class Orc extends Enemy {

	private static int attackDelay = 5000;

	public Orc() {
		super(160, 160, "Clumsy Orc", attackDelay, 'e');
	}

	public void effect(Entity target) {
		// Check if the reference to health works
		target.changeHealth(-getHealth() / 8);
		// Starts at 40 damage, decreases as Clumsy Orc loses health
	}

	public void effect(Entity[] e) {
		return;
	}

	public Entity copy() {
		return new Orc();
	}
}
