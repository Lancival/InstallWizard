package installwizard.entity;

import installwizard.spell.Spell;
import installwizard.spell.MyName;
import installwizard.spell.MyWork;
import installwizard.spell.Decay;
import installwizard.spell.ColossalWreck;

public class Ozymandias extends Wizard {
    
    private static Spell[] spellBook = {new MyName(), new MyWork(), new Decay(), new ColossalWreck()};

    public Ozymandias() {
        super(180, 180, "Ozymandias, King of Kings", spellBook);
    }

    public Entity copy() {
        return new Ozymandias();
    }
}
