package installwizard.entity;

import installwizard.spell.Spell;
import installwizard.spell.DiscoInferno;
import installwizard.spell.StayinAlive;
import installwizard.spell.DaftHands;
import installwizard.spell.RollingThunder;

public class Rasputin extends Wizard {
    private static Spell[] spellBook = {new DiscoInferno(), new StayinAlive(), new DaftHands(), new RollingThunder()};

    public Rasputin() {
        super(130,130,"Rasputin, the Disco Bard", spellBook);
    }

    public Entity copy() {
    	return new Rasputin();

    }
}