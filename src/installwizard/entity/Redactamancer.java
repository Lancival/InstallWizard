package installwizard.entity;

import installwizard.spell.Spell;
import installwizard.spell.Redacted;

public class Redactamancer extends Wizard {
	
	private static Spell[] spellBook = {new Redacted()};

	public Redactamancer() {
		super(666, 666, "Washington Irving, the █████████████", spellBook);
	}

	public Entity copy() {
		return new Redactamancer();
	}
}