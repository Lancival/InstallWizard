package installwizard.entity;

import installwizard.spell.Spell;
import installwizard.spell.Calm_Embrace;
import installwizard.spell.Hypersonic_Assault;
import installwizard.spell.Empyrean_Spear;
import installwizard.spell.Midnight_Snack;

public class Sourcerer extends Wizard {
	
	private static Spell[] spellBook = {new Calm_Embrace(), new Hypersonic_Assault(), new Empyrean_Spear(), new Midnight_Snack()};

	public Sourcerer() {
		super(180, 180, "Coin the Sourcerer", spellBook);
	}

	public Entity copy() {
		return new Sourcerer();
	}
}