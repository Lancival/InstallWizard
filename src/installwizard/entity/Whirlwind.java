package installwizard.entity;

public class Whirlwind extends Enemy {

	private static int attackDelay = 750;

	public Whirlwind() {
		super(50, 50, "Whirlwind", attackDelay, 'a');
	}

	public void effect(Entity e) {
		return;
	}

	public void effect(Entity[] targets) {
		for (Entity target : targets) target.changeHealth(-1);
	}

	public Entity copy() {
		return new Whirlwind();
	}
}