package installwizard.entity;

import installwizard.spell.Component;
import installwizard.spell.Spell;

public class Witch extends Wizard {
    
    private static Spell[] spellBook = {new Component()};

    public Witch() {
        super(100, 100, "Granny Weatherwax", spellBook);
    }

    public Entity copy() {
    	return new Witch();
    }
}
