package installwizard.entity;

import installwizard.spell.Spell;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class Wizard extends Entity {

	protected Spell page; //Wizard's current spell being cast
	protected Spell[] spellBook; //List of spells wizard can cast
	protected ArrayList<int[]> boons = new ArrayList<>(); //boon[0]=buff amount, boon[1]=buff duration

	public Wizard(int health, int maxHealth, String name, Spell[] pages) {
		super(health, maxHealth, name);
		spellBook = pages;
	}

	public Spell getPage() {
	    //Returns the current spell being cast
		return page;
	}

	public Wizard(Wizard w) {
		super(w.getHealth(), w.getMaxHealth(), w.getName());
		spellBook = Arrays.copyOf(w.getSpellBook(), w.getSpellBook().length);
	}

	public void setPage() {
		page = spellBook[(int) Math.floor((Math.random()*spellBook.length))].copy();
	}
	public void setPage(Spell selected) {
	    //Sets the current spell to the spell provided
		page = selected;
	}

	public void tick() {
		//keeps track of boons to coef
		super.tick();
		for (int[] boon : boons) {
			boon[1]--;
			if (boon[1] <= 0) for (Spell apage : spellBook) apage.coef -= boon[0];
		}
		for (int i = boons.size() - 1; i >= 0; i--) if (boons.get(i)[1] <= 0) boons.remove(i);
	}

	public void addBoons(int amount, int duration) {
		//adds a boon to the wizard with the amount and duration (sec) provided
		for (Spell apage : spellBook) apage.coef += amount;
		boons.add(new int[]{amount,duration*40});
	}

	public Spell[] getSpellBook() {
	    //Returns the list of spells the wizard can cast
		return spellBook;
	}
}