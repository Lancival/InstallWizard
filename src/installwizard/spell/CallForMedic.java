package installwizard.spell;

import installwizard.entity.Entity;

public class CallForMedic extends Spell {

	private static String[] sequence = {"I NEED HEALING"};
	private static String[] flavor = {"Call for your medic: "};

	public CallForMedic() {
		super(sequence, flavor, 's', 1, "Call for your medic");
	}

	public void effect(Entity target) {
		target.changeHealth(5+(coef*15));
	}

	public void effect(Entity[] team) {
        return;
    }

    public Spell copy() {
        return new CallForMedic();
    }

}