package installwizard.spell;

import installwizard.entity.Entity;
import installwizard.entity.Enemy;

public class Calm_Embrace extends Spell {
	
	private static String[] flavor = {"You hold out the ", " and conjure the imp within. Holding the fire cracker out, you ask it to hover in place. You take your staff and hold it at its end and pull back in a batting fashion. You take a ", " at the fire cracker, and blasting it, cause it to explode into a thousand ", "."};
	private static String[] sequence = {"fire cracker", "mighty swing", "multicoloured butterflies"};

	public Calm_Embrace() {
		super(sequence, flavor, 'e', 1, "Calm Embrace of Illusionary Beauty");
	}

	public void effect(Entity target) {
		((Enemy) target).setTime(-10000);
	}

	public void effect(Entity[] e) {
		return;
	}

	public Spell copy() {
		return new Calm_Embrace();
	}
}