package installwizard.spell;

import installwizard.entity.Entity;

public class ColossalWreck extends Spell {

    private static String[] sequence = {"boundless and bare"};
    private static String[] flavor = {"...Of that colossal Wreck, ", "..."};

    public ColossalWreck() {
        super(sequence, flavor, 'e', 1, "Colossal Wreck");
    }

    public void effect(Entity e) {
        e.changeHealth(-7+(int)((e.getMaxHealth()-e.getHealth())*(-0.70)*coef));
    }

    public void effect(Entity[] e) {
        return;
    }

    public Spell copy() {
        return new ColossalWreck();
    }
}
