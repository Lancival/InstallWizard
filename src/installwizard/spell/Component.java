package installwizard.spell;

import installwizard.entity.Entity;

public class Component extends Spell {

	private String[] colors = {"maroon", "red", "yellow", "orange", "green", "cyan", "blue", "azure", "indigo", "violet", "purple", "magenta", "pink", "brown", "white", "grey", "gray", "black", "octarine"};
	private String[] materials = {"dust", "powder", "incense", "droppings", "fluff"};
	private static String[] flavor = {"Put the ", " into the cauldron."};
	private static String[] sequence = {"maroon dust"};

	public Component() {
		super(sequence, flavor, 'e', 1, "Component");
	}

	public void getNew() {
		String color = colors[(int)(colors.length * Math.random())];
		String material = materials[(int) (materials.length * Math.random())];
		sequence = new String[]{color + " " + material};
		int x = (int) (4.0 * Math.random());
		if (x == 0) target = 'e';       // enemy
		else if (x == 1) target = 's';   // self
		else if (x == 2) target = 't';   // team (all allies)
		else if (x == 3) target = 'a';   // ally
	}

	public void effect (Entity e) {
		if (target == 'e') e.changeHealth(coef*(int)((int) 10*Math.random()));
		else if (target == 's') e.changeHealth(coef*(int)(10 + (int) 20*Math.random()));
	}

	public void effect(Entity[] targets) {
		if (target == 't') for (Entity e : targets) e.changeHealth(coef*(int)(20 + (int) 20*Math.random()));
		else if (target == 'a') for (Entity e : targets) e.changeHealth(coef*(int)(10 + (int) 20*Math.random()));
	}

	public Spell copy() {
		getNew();
		return new Component();
	}
}