package installwizard.spell;

import installwizard.entity.Entity;
import installwizard.entity.Wizard;

public class DaftHands extends Spell {
    
    private static String[] sequence = {"harder better faster stronger"};
    private static String[] flavor = {"Daft Punk plays: ", ""};

    public DaftHands() {
        super(sequence, flavor, 't', 1, "Hands of the Daft");
    }

    public void effect(Entity[] team) {
        for (Entity w : team) ((Wizard) w).addBoons(1,10);
    }
    
    public void effect(Entity e) {
        return;
    }
    
    public Spell copy() {
        return new DaftHands();
    }
}
