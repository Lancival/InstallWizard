package installwizard.spell;

import installwizard.entity.Entity;

public class Decay extends Spell{

    private static String[] sequence = {"round the decay"};
    private static String[] flavor = {"Nothing beside remains. ", "..."};

    public Decay() {
        super(sequence, flavor, 'e', 1, "Decay");
    }

    public void effect(Entity e) {
        e.addDot(5*coef, 10);
    }

    public void effect(Entity[] e) {
        return;
    }

    public Spell copy() {
        return new Decay();
    }
}
