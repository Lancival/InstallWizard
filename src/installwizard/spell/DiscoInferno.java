package installwizard.spell;

import installwizard.entity.Entity;

public class DiscoInferno extends Spell {

	private static String[] flavor = {"The Trammps play: (", ") burn that mother down"};
	private static String[] sequence = {"burn baby burn"};

	public DiscoInferno() {
		super(sequence, flavor, 'e', 1, "Disco Inferno");
	}

	public void effect(Entity target) {
		target.addDot(10*coef,4);
	}

	public void effect(Entity[] e) {
		return;
	}

	public Spell copy() {
        return new DiscoInferno();
    }
}