package installwizard.spell;

import installwizard.entity.Entity;

public class Empyrean_Spear extends Spell {

	private static String[] flavor = {"You think of a deep, dark cave. You ", ". You hurl a torch straight up into the air. You ", " to get a good ground. You loudly exlaim: "};
	private static String[] sequence = {"dream of bats", "wiggle your toes", "Let there be light!"};

	public Empyrean_Spear() {
		super(sequence, flavor, 'e', 1, "Nargl'frob's Empyrean Spear");
	}
	
	public void effect(Entity e) {
		e.changeHealth(-200);
	}

	public void effect(Entity[] targets) {
		return;
	}

	public Spell copy() {
		return new Empyrean_Spear();
	}
}