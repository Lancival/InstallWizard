package installwizard.spell;

import installwizard.entity.Entity;

public class FlameSlash extends Spell {

	private static String[] sequence = {"a wave of fire"};
	private static String[] flavor = {"You slash upwards with your katana, sending ", " surging towards the enemy."};
    
	public FlameSlash() {
		super(sequence, flavor, 'e', 1, "Slash of the Red Lotus");
	}

	public void effect(Entity target) {
		target.changeHealth(-15-(22*coef));
	}

	public void effect(Entity[] team) {
        return;
    }

    public Spell copy() {
        return new FlameSlash();
    }
}