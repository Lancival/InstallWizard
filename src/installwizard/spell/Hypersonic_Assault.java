package installwizard.spell;

import installwizard.entity.Entity;

public class Hypersonic_Assault extends Spell {

	private static String[] flavor = {"You ", " and tap the top of the jar of mandrake roots, sensing their potential. You ", " into two lumps and stick a lump in each ear. You gingerly open the jar and ", " of binding on the roots. Swirling one finger in the air above the jar, you ", ". The conical cyclone begins to amplify the screaming of the mandrake roots."};
	private static String[] sequence = {"check the best-before date", "squish the candle", "mutter a quick oath", "form a small cyclone"};

	public Hypersonic_Assault() {
		super(sequence, flavor, 'a', 1, "Effermhor's Hypersonic Assault");
	}
	
	public void effect(Entity e) {
		return;
	}

	public void effect(Entity[] targets) {
		for (Entity e : targets) e.changeHealth(-70-(coef*30));
	}

	public Spell copy() {
		return new Hypersonic_Assault();
	}
}