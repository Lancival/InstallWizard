package installwizard.spell;

import installwizard.entity.Entity;

public class Illustration extends Spell {

	private static String[] flavor = {"You make some generic wizardly gestures and chant the magic words: " , "."};
	private static String[] sequence = {"Type these words to cast a spell"};

	public Illustration() {
		super(sequence, flavor, 's', 1, "Illustration");
	}

	public void effect(Entity target) {
		target.changeHealth(-35-(coef*20));
	}

	public void effect(Entity[] e) {
		return;
	}

	public Spell copy() {
        return new Illustration();
    }
}