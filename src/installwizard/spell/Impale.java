package installwizard.spell;

import installwizard.entity.Entity;

public class Impale extends Spell {

	private String[] bodyparts = {"head", "neck", "throat", "arms", "side", "chest", "guts", "legs"};
	private static String[] sequence = {"powerful attack", "pierces", "thorax"};
	private static String[] flavor = {"You launch a ", ". Your victim attempts to say some words as your katana ", " its "};

	public Impale() {
		super(sequence, flavor, 'e', 1, "Impale");
	}

	private void randomize() {
		String randomPart = bodyparts[(int) (bodyparts.length * Math.random())];
		sequence = new String[] {"powerful attack", "pierces", randomPart + ": Gurgle gurgle aaaaargh."};
	}

	public void effect (Entity target) {
		target.changeHealth(-100);
	}

	public void effect (Entity[] targets) {
		return;
	}

	public Spell copy() {
		randomize();
		return new Impale();
	}

}