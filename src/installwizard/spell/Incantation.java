package installwizard.spell;

import installwizard.entity.Entity;

public class Incantation extends Spell {

    private String[] syllables = {"raan", "mir", "tah", "laas", "yah", "nir", "mid", "vur", "shaan", "feim", "zii", "gron", "gol", "hah", "dov", "od", "ah", "viing", "hun", "kaal", "zoor", "lok", "vah", "koor", "ven", "gaar", "nos", "zun", "haal", "viik", "faas", "ru", "maar", "mul", "qah", "diiv", "joor", "zah", "frul", "gaan", "lah", "haas", "su", "grah", "dun", "yol", "toor", "shul", "fo", "krah", "diin", "liz", "slen", "nus", "kaan", "drem", "ov", "krii", "lun", "aus", "rii", "vaaz", "zol", "tiid", "klo", "ul", "strun", "bah", "qo", "dur", "neh", "viir", "zul", "mey", "gut", "fus", "ro", "dah", "wuld", "nah", "kest", "zii", "los", "dii", "du", "slen", "tiid", "vo", "nahl", "dal", "vus", "diil", "qoth", "zaarn", "ven", "mul", "riik", "fiik", "lo", "sah"};

    public Incantation() {
        super(new String[]{"placeholder"},new String[]{"Shout "} , 'e', 1, "Incantation");
        randomize();
    }

    public Incantation(int i) {
        super(new String[]{"fus roh dah"}, new String[]{"Shout "} , 'e', 1, "Incantation");
    }

    private void randomize() {
        sequence = new String[1];
        sequence[0] = syllables[(int) ((syllables.length) * Math.random())];

        for (int i = 0; i < 2; i++) sequence[0] += " " + syllables[(int) ((syllables.length) * Math.random())];
        int x = (int) (4.0 * Math.random());
        if (x == 0) target = 'e'; // enemy
        else if (x == 1) target = 's'; // self
        else if (x == 2) target = 't'; // team (all allies)
        else if (x == 3) target = 'a'; // ally
        keywords=sequence[0];
    }

    public void effect (Entity t) {
        if (sequence[0] == "fus roh dah") t.changeHealth(-100);
    	else if (target == 'e') t.changeHealth(-(10*coef) - (int) (15*Math.random()));
    	else if (target == 's') t.changeHealth((10*coef) + (int) (30*Math.random()));
    	else if (target == 'a') t.changeHealth(10 + (int) (10*Math.random()));
    }

    public void effect (Entity[] targets) {
        if (target == 't') for (Entity t : targets) t.changeHealth((int) (coef*40*Math.random()));
    }

    public Spell copy() {
        randomize();
        return new Incantation();
    }
}
