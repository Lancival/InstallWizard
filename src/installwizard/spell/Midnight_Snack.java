package installwizard.spell;

import installwizard.entity.Entity;

public class Midnight_Snack extends Spell {

	private static String[] flavor = {"You close your eyes and ", ". You think about a nice big dinner; pigs with apples, cakes and desserts. You open your eyes and spread your arms, ", " with an inviting gaze."};
	private static String[] sequence = {"contemplate the idea of food", "welcoming the food"};

	public Midnight_Snack() {
		super(sequence, flavor, 's', 1, "Wurphle's Midnight Snack");
	}
	
	public void effect(Entity e) {
		e.changeHealth(25+30*coef);
	}

	public void effect(Entity[] targets) {
		return;
	}

	public Spell copy() {
		return new Midnight_Snack();
	}
}