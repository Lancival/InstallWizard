package installwizard.spell;

import installwizard.entity.Entity;

public class MyName extends Spell {

    private static String[] sequence = {"King of Kings"};
    private static String[] flavor = {"My name is Ozymandias, ", ";"};
    
    public MyName() {
        super(sequence, flavor, 'e', 1, "Name of Mine");
    }

    public void effect(Entity target) {
        target.changeHealth(-25*coef);
    }

    public void effect(Entity[] e) {
        return;
    }

    public Spell copy() {
        return new MyName();
    }
}
