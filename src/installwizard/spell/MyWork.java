package installwizard.spell;

import installwizard.entity.Entity;

public class MyWork extends Spell {
    
    private static String[] sequence = {"ye mighty and despair"};
    private static String[] flavor = {"Look on my Works, ", "!"};

    public MyWork() {
        super(sequence, flavor, 'e', 1, "Work of Mine");
    }

    public void effect(Entity e) {
        e.changeHealth((int)(e.getHealth()*(-0.32)*coef));
    }

    public void effect(Entity[] e) {
        return;
    }

    public Spell copy() {
        return new MyWork();
    }
}
