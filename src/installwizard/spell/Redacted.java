package installwizard.spell;

import installwizard.entity.Entity;

public class Redacted extends Spell {
	
	private static String[] characters = {"`","1","2","3","4","5","6","7","8","9","0","-","=","q","w","e","r","t","y","u","i","o","p","[","]","\\","a","s","d","f","g","h","j","k","l",";","\'","z","x","c","v","b","n","m",",",".","/","~","!","@","#","$","%","^","&","*","(",")","_","+","Q","W","E","R","T","Y","U","I","O","P","{","}","|","A","S","D","F","G","H","J","K","L",":","\"","Z","X","C","V","B","N","M","<",">","?"};
	private static String[] flavor = {"███ ██████ ████████ ██████ as ███ ██████ the █████ █████: "};
	private static String[] sequence = {"placeholder"};

	public Redacted() {
		super(sequence, flavor, 'a', 1, "Redacted");
	}

	public void effect(Entity e) {
		return;
	}

	public void effect(Entity[] targets) {
		for (Entity e : targets) e.changeHealth(-1*e.getMaxHealth());
	}

	public Spell copy() {
		sequence = new String[1];
		sequence[0] = "";
		for (int i=0; i<8; i++) sequence[0] += characters[(int) Math.floor(Math.random()*characters.length)];
		return new Redacted();
	}

	@Override
	public String[] getSequence() {
		String[] s = new String[1];
		s[0] = "████████";
		return s;
	}
}