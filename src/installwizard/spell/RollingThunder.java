package installwizard.spell;

import installwizard.entity.Entity;

public class RollingThunder extends Spell {

  private static String[] flavor = {"Rick Astley plays: ", ", never gonna let you down, never gonna run around and desert you!"};
  private static String[] sequence = {"never gonna give you up"};

  public RollingThunder() {
    super(sequence, flavor, 'e', 1, "Rolling Thunder");
  }

  public void effect (Entity target) {
    target.changeHealth(-(int)(target.getHealth()*0.40));
  }

  public void effect(Entity[] e) {
    return;
  }

  public Spell copy() {
    return new RollingThunder();
  }
}