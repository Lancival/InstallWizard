package installwizard.spell;

import java.util.*;
import installwizard.entity.Entity;

public abstract class Spell {
    
    protected String[] flavor; //Flavor text describing the spell
    protected String[] sequence; //Set of keywords that must be typed to cast the spell
    protected String keywords; //Sequence condensed into a single String
    protected int rem = 0; //int that tracks how much of the keywords has been typed
    protected char target; //Type of spell (what it targets)
    public int coef; //Can be changed to manipulate strength of spell
    protected String name; //Name of spell

    /*
    Target Types
    e: enemy
    s: self
    t: team
    a: all enemies
    d: self and enemy
    r: random
    */

    public Spell(String[] s, String[] f, char t, int c, String n) {
        sequence = s;
        flavor = f;
        target = t;
        coef = c;
        name = n;
        keywords = Arrays.toString(sequence);
        keywords = keywords.substring(1,keywords.length()-1).replaceAll(", ","");
    }

    public int keyed() {
        //Returns how much of the keywords has been typed
        return rem;
    }

    public boolean next(char stroke) {
        //Checks if the character typed is the next character of the keywords, returns whether the spell is completed
        if (rem >= keywords.length()) return false;
        boolean match = (stroke == keywords.charAt(rem));
        if (match) rem++;
        if (rem < keywords.length() && keywords.substring(rem,rem+1).equals(" ")) rem++;
        return isComplete();
    }

    public boolean isComplete() {
        //Returns whether the spell is completed
        boolean complete = (rem == keywords.length());
        return (complete);
    }

    public char getTarget() {
        //Returns what the spell targets
        return target;
    }

    public String[] getSequence() {
        //Returns the spell's sequence
        return sequence;
    }

    public String[] getFlavor() {
        //Returns the spell's flavor text
        return flavor;
    }

    public String getName() {
        //Returns the spell's name
        return name;
    }

    public abstract void effect(Entity e); //Method that causes a change in an Entity (the spell's effect)
    public abstract void effect(Entity[] e); //Method that causes a change in multiple Entities (the spell's effect)
    public abstract Spell copy(); //Factory method used to copy this spell
}
