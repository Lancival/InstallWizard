package installwizard.spell;

import installwizard.entity.Entity;

public class StayinAlive extends Spell {

    private static String[] sequence = {"life goin' nowhere. somebody help me"};
    private static String[] flavor = {"Bee Gees plays: ", "."};

    public StayinAlive() {
        super(sequence, flavor, 't', 5, "Staying Alive");
    }

    public void effect(Entity e) {
    	return;
    }

    public void effect(Entity[] team) {
        for (Entity w : team) w.changeHealth(20+20*coef);
    }

    public Spell copy() {
        return new StayinAlive();
    }
}